import { createContext, useState } from "react";

export const MovieContext = createContext();

export const MoviePRovider = props => {
  const [movies, setMovies] = useState([
    {
      name: "Harry Potter",
      price: "$10",
      id: 1234
    },
    {
      name: "Game Of Thrones",
      price: "$10",
      id: 1242334
    },
    {
      name: "inception",
      price: "$10",
      id: 2314
    }
  ]);
  return (
    <MovieContext.Provider value={[movies, setMovies]}>
      {props.children}
    </MovieContext.Provider>
  );
};
