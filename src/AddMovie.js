import React, { useContext, useState } from "react";
import { MovieContext } from "./MovieContext";

const AddMovie = props => {
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [movie, setMovies] = useContext(MovieContext);

  const updateName = e => {
    setName(e.target.value);
  };

  const updatePrice = e => {
    setPrice(e.target.value);
  };

  const addMovie = e => {
    e.preventDefault();
    setMovies(prevMovies => [...prevMovies, { name: name, price: price }]);
  };

  return (
    <form onSubmit={addMovie}>
      <p> Movie Name:</p>
      <input type="text" name="name" value={name} onChange={updateName} />

      <p>Price:</p>
      <input type="text" name="price" value={price} onChange={updatePrice} />
      <hr></hr>
      <button>Submit</button>
    </form>
  );
};

export default AddMovie;
