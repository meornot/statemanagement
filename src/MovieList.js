import React, { useContext, useState } from "react";
import { MovieContext } from "./MovieContext";
import Movies from "./Movies";

const MovieList = () => {
  const [movies, setMovies] = useContext(MovieContext);
  return (
    <div>
      {movies.map(movie => (
        <Movies name={movie.name} price={movie.price} key={movie.id} />
      ))}
    </div>
  );
};
export default MovieList;
