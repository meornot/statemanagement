import AddMovie from "./AddMovie";
import "./App.css";
import { MoviePRovider } from "./MovieContext";
import MovieList from "./MovieList";
import Nav from "./Nav";

function App() {
  return (
    <MoviePRovider>
      <div className="App">
        <Nav />
        <AddMovie />
        <MovieList />
      </div>
    </MoviePRovider>
  );
}

export default App;
